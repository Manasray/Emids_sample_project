package com.emids.persister;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.emids.dao.NormanGameDao;

@Repository
public class NormanGamePersister {

	private static final String AUTH_QRY="INSERT INTO healthinsurance(name,age,gender,insuranceValue) VALUES(?,?,?,?) ";
	@Autowired
	private JdbcTemplate jt;
	
	public int Saving(NormanGameDao dao) {
		int update = jt.update(AUTH_QRY, dao.getNameDao(),dao.getAgeDao(),dao.getGenderDao(),dao.getAmount());
		return update;
	}
}
