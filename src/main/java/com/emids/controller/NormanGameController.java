package com.emids.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emids.bo.NormanGame;
import com.emids.service.NormanGameService;

@Controller
public class NormanGameController {
	
	@Autowired
	NormanGameService  service; 
	
	public NormanGameController() {
		System.out.println("NormanGameController.NormanGameController()");
	}

	@RequestMapping(value = "/performsearch.htm", method = RequestMethod.POST)
	public @ResponseBody String validate(HttpServletRequest req, @ModelAttribute NormanGame model) {

		return  this.service.calculateAmount(model);

	}
}
