package com.emids.dao;

public class NormanGameDao {

	private String nameDao;
	private String genderDao;
	private Integer ageDao;
	private String[] healthDao;
	private String[] habitsDao;
	private double amount;
	
	public NormanGameDao() {
	}

	public NormanGameDao(String nameDao, String genderDao, Integer ageDao, String[] healthDao, String[] habitsDao,
			double amount) {
		super();
		this.nameDao = nameDao;
		this.genderDao = genderDao;
		this.ageDao = ageDao;
		this.healthDao = healthDao;
		this.habitsDao = habitsDao;
		this.amount = amount;
	}

	public String getNameDao() {
		return nameDao;
	}

	public void setNameDao(String nameDao) {
		this.nameDao = nameDao;
	}

	public String getGenderDao() {
		return genderDao;
	}

	public void setGenderDao(String genderDao) {
		this.genderDao = genderDao;
	}

	public Integer getAgeDao() {
		return ageDao;
	}

	public void setAgeDao(Integer ageDao) {
		this.ageDao = ageDao;
	}

	public String[] getHealthDao() {
		return healthDao;
	}

	public void setHealthDao(String[] healthDao) {
		this.healthDao = healthDao;
	}

	public String[] getHabitsDao() {
		return habitsDao;
	}

	public void setHabitsDao(String[] habitsDao) {
		this.habitsDao = habitsDao;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NormanGameDao [nameDao=");
		builder.append(nameDao);
		builder.append(", genderDao=");
		builder.append(genderDao);
		builder.append(", ageDao=");
		builder.append(ageDao);
		builder.append(", healthDao=");
		builder.append(healthDao);
		builder.append(", habitsDao=");
		builder.append(habitsDao);
		builder.append(", amount=");
		builder.append(amount);
		builder.append("]");
		return builder.toString();
	}



}
