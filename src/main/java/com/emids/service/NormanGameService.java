package com.emids.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emids.bo.NormanGame;
import com.emids.dao.NormanGameDao;
import com.emids.persister.NormanGamePersister;

@Service
public class NormanGameService {

	@Autowired
	NormanGamePersister persist;

	private double calculatePremium(NormanGame game) {

		double amount = 5000d;
		double calculateAmount = 5000d;
		Integer age = game.getAge();

		if (age > 25) {

			int diff = age - 25;
			double ceil = Math.ceil(diff / 5.0f);

			calculateAmount = (amount + amount * 10 / 100);
			int i = 0;

			for (int j = 0; j < (int) ceil; j++) {

				if (i < 3) {
					calculateAmount = calculateAmount + calculateAmount * 10 / 100;
				} else {
					calculateAmount = calculateAmount + calculateAmount * 20 / 100;
				}

				i++;
			}
		} else {
			calculateAmount = (amount + amount * 10 / 100);
		}

		if (game.getGender().equals("male")||game.getGender().equals("female")||game.getGender().equals("others")) {
			calculateAmount = calculateAmount + calculateAmount * 2 / 100;
		}

		String[] healths = game.getHealth();
		for (String health : healths) {
			calculateAmount = calculateAmount + calculateAmount * 1 / 100;
		}

		String[] habits = game.getHabits();

		int habbitPer = 0;

		for (String habit : habits) {
			if (habit.equals("daily_exercise")) {
				habbitPer = habbitPer - 3;
			} else {
				habbitPer = habbitPer + 3;
			}
		}

		if (habbitPer != 0) {
			int abs = Math.abs(habbitPer);

			if (habbitPer > 0) {
				calculateAmount = calculateAmount + calculateAmount * abs / 100;
			} else {
				calculateAmount = calculateAmount - calculateAmount * abs / 100;

			}

		}

		calculateAmount = Math.ceil(calculateAmount);
		return calculateAmount;

	}

	public String calculateAmount(NormanGame game) {
		NormanGameDao dao = new NormanGameDao();
		double calculatePremium = new NormanGameService().calculatePremium(game);

		dao.setNameDao(game.getName());
		dao.setAgeDao(game.getAge());
		dao.setHabitsDao(game.getHabits());
		dao.setHealthDao(game.getHealth());
		dao.setGenderDao(game.getGender());
		dao.setAmount(calculatePremium);
		int saving = persist.Saving(dao);
		if (saving > 0) {
			System.out.println("data saved");
		} else
			System.out.println("something wrong happen PLZ check");

		return "Health Insurance Premium for " + game.getName() + " : Rs. " + calculatePremium;
	}
}
